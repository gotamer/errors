package e

import (
	"fmt"
	"testing"

	"bitbucket.org/gotamer/mail"
)

func TestNoError(t *testing.T) {
	if ok := Check(nil); ok != true {
		t.Errorf("TestNoError")
	}
}

func TestError(t *testing.T) {
	err := fmt.Errorf("Test Error error")
	if ok := Check(err); ok == true {
		t.Errorf("TestError: %s", err.Error())
	}
}

func TestMailError(t *testing.T) {
	ENVIRONMENT = ENV_PROD
	s := new(mail.Smtp)
	s.SetHostname("smtp.gmail.com")
	s.SetHostport(587)
	s.SetFromName("GoTamer")
	s.SetFromAddr("xxxxxxxx@gmail.com")
	s.SetPassword("********")
	s.SetToAddrs("tamer@riky.net")
	s.SetSubject("GoTamer test error mail")
	MailLogger(s)
	err := fmt.Errorf("Testing mail error")
	if ok := Check(err); ok == true {
		t.Errorf("%s", err.Error())
	}
}
