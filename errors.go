// Package (e) errors implements functions to manipulate errors.
// It also logs them to files and email errors in production mode
//
package e

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

type Env uint8

const (
	DEFAULT_ENV_LOG_LEVEL = "GO_LOG_LEVEL"

	ENV_PROD Env = iota // Environment
	ENV_BETA
	ENV_DEBG
)

/*
var priorityString = map[priority]string{
	LOG_EMERG:   "EMER",
	LOG_ALERT:   "ALER",
	LOG_CRIT:    "CRIT",
	LOG_ERR:     "ERRO",
	LOG_WARNING: "WARN",
	LOG_NOTICE:  "NOTZ",
	LOG_INFO:    "INFO",
	LOG_DEBUG:   "DEBG",
}
*/
var (
	// Key name to set the log level
	ENV_LOG_LEVEL = DEFAULT_ENV_LOG_LEVEL

	// Key name to set the Context Tag
	ENV_LOG_CONTEXT string
	ENVIRONMENT     Env = ENV_DEBG

	LogError *log.Logger
	LogInfo  *log.Logger
	LogDebug *log.Logger
	LogEmail *log.Logger

	safeWriter io.Writer
)

func init() {
	Logger(os.Stderr)
}

// Set custom environment to control output log level.
func AppName(name string) {
	ENV_LOG_LEVEL = fmt.Sprintf("%s_LOG_LEVEL", strings.ToUpper(name))
}

func env() {
	envLevel := os.Getenv(ENV_LOG_LEVEL)

	if envLevel == "DEBG" {
		Logger(os.Stderr)
	} else {
		Logger(safeWriter)
	}
}

// Logger sets the output destination for the custom loggers.
func Logger(to io.Writer) {
	if to != os.Stderr {
		safeWriter = to
	}
	LogError = log.New(to, "[ERRO] ", 19)
	LogInfo = log.New(to, "[INFO] ", 19)
	LogDebug = log.New(to, "[DEBG] ", 19)
}

func FileLogger(file string) {
	LogFile, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Printf("Error opening log file: \n\t%v\n", err)
		os.Exit(2)
	}

	// SetOutput sets the output destination for the standard logger.
	log.SetOutput(LogFile)
	// log.SetPrefix()
	Logger(LogFile)
}

// LogEmail can be used with bitbucket.org/gotamer/mail, since it implements io.Writter
func MailLogger(to io.Writer) {
	LogEmail = log.New(to, "[ERROR] ", 19)
}

// Returns false on error
func Check(err error) bool {
	if err == nil {
		return true
	}
	check(err)
	return false
}

// Returns true on error
func Error(err error) bool {
	if err == nil {
		return false
	}
	check(err)
	return true
}

// Exit on error
func Fail(err error) {
	if err != nil {
		check(err)
		os.Exit(1)
	}
}

// Print DEBUG info
func Debug(context, format string, v ...interface{}) {
	if ENVIRONMENT == ENV_DEBG {
		envContext := os.Getenv(ENV_LOG_CONTEXT)
		if context == envContext || envContext == "" {
			var ctx string
			if envContext != "" {
				ctx = fmt.Sprintf("[%s] ", context)
				LogDebug.SetPrefix(ctx)
			}
			LogDebug.Output(2, fmt.Sprintf(format, v...))
			LogDebug.SetPrefix("")
		}
	}
}

// Print out anything while not in production
func Info(format string, v ...interface{}) {
	if ENVIRONMENT != ENV_DEBG {
		LogInfo.Output(2, fmt.Sprintf(format, v...))
	}
}

func check(err error) {
	if err != nil {
		switch ENVIRONMENT {
		case ENV_DEBG:
			LogDebug.Output(3, err.Error())
			break
		case ENV_PROD:
			LogError.Output(3, err.Error())
			if LogEmail != nil {
				if err := LogEmail.Output(3, err.Error()); err != nil {
					log.Printf("Log::Email: %s\n", err.Error())
				}
			}
		}
	}
}

//####### Standard Go Error #############
type errorString struct {
	s string
}

// New returns a standard go error that formats as the given text.
func New(text string) error {
	return &errorString{text}
}

func (e *errorString) Error() string {
	return e.s
}

// Turns a standard go error in to a Context error.
func (e *errorString) Ctx(ctx string) error {
	return &errorContext{ctx, e.Error()}
}

//####### Context Error #############
type errorContext struct {
	context string
	text    string
}

// Returns a Context Error.
func NewCtxErr(ctx, text string) error {
	return &errorContext{ctx, text}
}

// Returns a context error as a formated string
func (e *errorContext) Error() string {
	return fmt.Sprintf("[%v] %v", e.context, e.text)
}
