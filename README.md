bitbucket.org/gotamer/errors
============================

[![GoDoc](https://godoc.org/bitbucket.org/gotamer/errors?status.png)](https://godoc.org/bitbucket.org/gotamer/errors)


Package (e) errors implements functions to manipulate errors in Go

It also logs them to files and email errors in production mode

